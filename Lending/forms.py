from dataclasses import fields
from pyexpat import model
from trace import Trace
from django import forms
from django.db import models
from django.forms import ModelForm, inlineformset_factory
from .models import Equipments,LendingHistory,Tag
from django.forms.models import inlineformset_factory
import datetime



from Profile.models import Profile

class RollnoForm(forms.Form):
    roll_no=forms.CharField(max_length=30)

class EquipmentsForm(forms.ModelForm):
    class Meta:
        model=Equipments
        fields='__all__'
    def __init__(self,*args,**kwargs):
        super(EquipmentsForm,self).__init__(*args,**kwargs)
        self.fields['tag'].widget.attrs.update({'class':'select-tag'})

class LendingHistoryForm(ModelForm):
    class Meta:
        model=LendingHistory
        fields=['item','quantity','tentative_return_date']
        widgets = {
        'tentative_return_date': forms.DateInput(attrs={ 'type':'date','min':datetime.date.today()}),
        'quantity':forms.NumberInput(attrs={'min':1})
    }

    def __init__(self,*args,**kwargs):
        super(LendingHistoryForm,self).__init__(*args,**kwargs)
        self.fields['item'].widget.attrs.update({'class':'select-item'})
        
        

LendingFormset=inlineformset_factory(
    Profile,
    LendingHistory,
    LendingHistoryForm,
    can_delete=False,
    min_num=2,
    extra=0
)

# class TagForm(ModelForm):
#     class Meta:
#         model=Tag
#         fields=['name']

#     def __init__(self,*args,**kwargs):
#         super(TagForm,self).__init__(*args,**kwargs)
#         self.fields['name'].widget.attrs.update({'class':'select-category'})

class TagForm(ModelForm):
    class Meta:
        model=Tag
        fields='__all__'


    



    