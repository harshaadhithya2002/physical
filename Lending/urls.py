from unicodedata import name
from django.urls import path
from . import views


urlpatterns = [

    path('',views.lendings,name='lendings'),
    path('<str:pk>/',views.issue_items,name='issue_items'),
    path('equipment_list',views.show_equipments,name='equipments_list'),
    path('create_equipment',views.create_equipment,name='create_equipment'),
    path('update_equipment/<str:pk>',views.edit_equipment,name='update_equipment'),
    
    path('history/<str:pk>',views.show_history,name='show_history'),
    #path('update_history/<str:pk>',views.update_history,name='update_history'),
    path('return-item/<str:pk>',views.return_item,name='return-item'),

    path('unreturned-items',views.unreturned_items,name='unreturned_items'),
    
    path('categories',views.categories,name='categories'),
    path('create_new_category',views.create_new_category,name='create_new_category')

]