
from multiprocessing import context
from profile import Profile
from django.http import HttpResponse
from django.shortcuts import redirect, render

from Profile.views import home
from .forms import RollnoForm,EquipmentsForm,LendingFormset,LendingHistoryForm,TagForm
from .models import Equipments, LendingHistory,Tag
from Profile.models import Profile

from django.forms import DateInput, inlineformset_factory,modelformset_factory
from .widgets import DatePickerInput,MinInteger
from django.db.models import Q

from django.contrib import messages

import datetime

def issue_items(request,pk):
    profile=Profile.objects.get(id=pk)
    lend_formset=modelformset_factory(LendingHistory,form=LendingHistoryForm,extra=10)
    formset=lend_formset(request.POST or None,queryset=LendingHistory.objects.none())
    if request.method=='POST':
        if formset.is_valid():
            
            history_objs=formset.save(commit=False)
            for history_obj in history_objs:
                history_obj.issued_by=profile
                try:
                    history_obj.save()
                    item_obj=history_obj.item
                    item_obj.available_stock-=history_obj.quantity
                    item_obj.save()
                except:
                    pass
            return redirect('show_history',pk=profile.id)
    context={'formset':formset}
    return render(request,'lending/lend_items.html',context)
        

"""def update_history(request,pk):
    lending_history_obj=LendingHistory.objects.get(id=pk)
    item=lending_history_obj.item
    old_quantity=lending_history_obj.quantity
    form=LendingHistoryForm(request.POST or None,instance=lending_history_obj)
    if request.method=='POST':
        item.available_stock+=old_quantity
        item.save()
        if form.is_valid():
            lending_history_obj=form.save(commit=False)
            item.available_stock-=lending_history_obj.quantity
            try:
                lending_history_obj.save()
                
            except:
                messages.error(request,'check the available stock and entered quantity')
            return redirect('show_history',pk=lending_history_obj.issued_by.id)
        else:
            item=lending_history_obj.item
            item.availabe_stock-=old_quantity
            item.save()
            messages.error(request,'check the available stock and entered quantity')

    context={'form':form,'lending_obj':lending_history_obj }
    return render(request,'lending/update_history.html',context)"""

"""def delete_history(request,pk):
    lending_history_obj=LendingHistory.objects.get(id=pk)
    item=lending_history_obj.item
    item.available_stock+=lending_history_obj.quantity
    item.save()
    lending_history_obj.delete()
    return HttpResponse('')"""
    
    

def show_equipments(request):
    equipments=Equipments.objects.all()
    context={'equipments':equipments,'page':'create'}
    return render(request,'lending/equipments.html',context)

def create_equipment(request):
    form=EquipmentsForm()
    if request.method=='POST':
        form=EquipmentsForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('equipments_list')
        else:
            messages.error(request,"Available quantity must be less than or equal to total stock") 
    context={'form':form}
    return render(request,'lending/update_equipment.html',context)

def edit_equipment(request,pk):
    equipment_obj=Equipments.objects.get(id=pk)
    form=EquipmentsForm(instance=equipment_obj)
    if request.method=='POST':
        form=EquipmentsForm(request.POST,instance=equipment_obj)
        if form.is_valid():
            form.save()
            return redirect('equipments_list')
        else:
            messages.error(request,'Available quantity must be less than or equal to total stock')
        
    context={'form':form,'page':'edit'}
    return render(request,'lending/update_equipment.html',context)


def lendings(request):
    form=RollnoForm
    context={}
    if request.method=='POST':
        roll_no=request.POST['roll_no']
        try:
            profile_id=Profile.objects.get(roll_no=roll_no).id
            return redirect('show_history',pk=profile_id)
        except:
            messages.error(request,"Roll no doesn't match") 
    context['form']=form
    return render(request,'lending/lending.html',context)

"""def issue_items(request,pk):
    profile_obj=Profile.objects.get(id=pk)
    extras=5
    items_formset=inlineformset_factory(Profile,LendingHistory,fields=('item','quantity','tentative_return_date'),can_delete=False,widgets={'tentative_return_date':DatePickerInput,'quantity':MinInteger},extra=extras)
    formset=items_formset(queryset=LendingHistory.objects.none(),instance=profile_obj)
    if request.method=='POST':
        formset=items_formset(request.POST,instance=profile_obj)
        if formset.is_valid():
            
            for i in range(0,extras):
                key_quantity='lendinghistory_set-{}-quantity'.format(i)
                key_item_id='lendinghistory_set-{}-item'.format(i)
                entered_quantity=request.POST[key_quantity]
                entered_item_id=request.POST[key_item_id]
                if not entered_item_id:
                    break
                
                equipment=Equipments.objects.get(id=entered_item_id)
                equipment.available_stock-=int(entered_quantity)
                
                equipment.save()
               
            formset.save()
        else:
            messages.error(request,"Quantity must be less than available stock ,Enter tentative date and it should be greater than today's date")
            return redirect('issue_items',pk=profile_obj.id)
        return redirect('lendings')
    context={'profile':profile_obj,'formset':formset}
    return render(request,'lending/issue_items.html',context)
"""


def show_history(request,pk):
    roll_no=Profile.objects.get(id=pk).roll_no
    profile_obj=Profile.objects.get(roll_no=roll_no)
    issued_equipments=LendingHistory.objects.filter(issued_by=profile_obj,is_returned=False)
    returned_equipments=LendingHistory.objects.filter(issued_by=profile_obj,is_returned=True)
    context={'roll_no':roll_no,'issued_equipments':issued_equipments,'returned_equipments':returned_equipments,'profile_id':profile_obj.id}
    return render(request,'lending/history.html',context)

def return_item(request,pk):
    issued_item=LendingHistory.objects.get(id=pk)
    issued_item.is_returned=True
    issued_item.actual_return_date=datetime.datetime.now()
    try:
        issued_item.save()
        issued_item.item.available_stock+=issued_item.quantity
        issued_item.item.save()
    except:
        messages.error(request,'Error in returning items')
    return redirect('show_history',pk=issued_item.issued_by.id)

def unreturned_items(request):
    today=datetime.date.today()
    unreturned=LendingHistory.objects.filter(is_returned=False,tentative_return_date__lt=today)
    all_items=LendingHistory.objects.filter()
    delayed_return=[]
    for item in all_items:
        if item.is_returned==True and (item.tentative_return_date<item.actual_return_date):
            item.day_difference=(item.actual_return_date-item.tentative_return_date).days
            delayed_return.append(item)
        elif item.is_returned==False and (item.tentative_return_date<today):
            item.day_difference=(today-item.tentative_return_date).days
            delayed_return.append(item)
    
    context={'unreturned':delayed_return}
    return render(request,'lending/unreturned.html',context)

def categories(request):
    categories=Tag.objects.all()
    context={'categories':categories}
    return render(request,'lending/categories.html',context)

def create_new_category(request):
    form=TagForm()
    
    if request.method=='POST':
        form=TagForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('categories')
        else:
            messages.error(request,'Category name should be unique')
    context={'form':form}
            
    return render(request,'lending/create_new_category.html',context)
