# Generated by Django 4.0.2 on 2022-02-24 09:49

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('Profile', '0002_alter_profile_dept_alter_profile_roll_no'),
        ('lending', '0002_remove_equipments_stock_equipments_available_stock_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='LendingHistory',
            fields=[
                ('quantity', models.IntegerField()),
                ('issued_date', models.DateTimeField(auto_now_add=True)),
                ('tentative_return_date', models.DateTimeField()),
                ('actual_return_date', models.DateTimeField()),
                ('is_returned', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True)),
                ('issued_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Profile.profile')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lending.equipments')),
            ],
        ),
    ]
