from django.contrib import admin
from .models import Equipments,LendingHistory,Tag

# Register your models here.
admin.site.register(Equipments)
admin.site.register(LendingHistory)
admin.site.register(Tag)