from curses.ascii import FS
from statistics import mode
from django.db import models
import uuid
from Profile.models import Profile

from django.core.exceptions import ValidationError
from django.utils import timezone

from datetime import datetime, timezone

from django.core.validators import MaxValueValidator, MinValueValidator 

    
  

    



# Create your models here.

class Equipments(models.Model):
    equipment_code=models.CharField(max_length=30,unique=True)
    equipment_name=models.CharField(max_length=100)
    brand=models.CharField(max_length=50,null=True,blank=True)
    total_stock=models.PositiveIntegerField()
    available_stock=models.PositiveIntegerField()
    tag=models.ManyToManyField('Tag',blank=True,null=True)
    created=models.DateTimeField(auto_now_add=True)
    id=models.UUIDField(default=uuid.uuid4,unique=True,primary_key=True,editable=False)

    def __str__(self):
        if self.brand:
            return '{}-{}'.format(self.brand,self.equipment_name)

        return self.equipment_name

    def clean(self):
        if self.available_stock>self.total_stock:
            raise ValidationError({'available_stock':'Available stock must be less than or equal to the total Stock'})

class Tag(models.Model):
    name=models.CharField(max_length=100,unique=True)
    created=models.DateTimeField(auto_now_add=True)
    id=models.UUIDField(default=uuid.uuid4,unique=True,primary_key=True,editable=False)

    def __str__(self):
        return self.name

class LendingHistory(models.Model):
    issued_by=models.ForeignKey(Profile,on_delete=models.CASCADE)
    item=models.ForeignKey(Equipments,on_delete=models.CASCADE)
    quantity=models.IntegerField(blank=False,validators=[MinValueValidator(1)])
    issued_date=models.DateField(auto_now_add=True)
    tentative_return_date=models.DateField(null=True,blank=True)
    actual_return_date=models.DateField(null=True,blank=True)
    is_returned=models.BooleanField(default=False)
    created=models.DateTimeField(auto_now_add=True)
    id=models.UUIDField(default=uuid.uuid4,unique=True,primary_key=True,editable=False)

    def __str__(self):
        return self.issued_by.roll_no

    def clean(self):
        if not self.quantity:
            raise ValidationError({'quantity':'Please enter quantity'})
        else:
            equipment=self.item
            if self.quantity>equipment.available_stock:
                raise ValidationError({'quantity':'quantity must be lesser than the available stock'})
    """def clean(self):
        #now = datetime.now(timezone.utc)
        #if not self.issued_date:
         #   raise ValidationError({'tentative_return_date':'Enter Tentative Date'})
        if self.quantity==0:
            raise ValidationError({
                'quantity':'0 quantity error'
            })
        if self.quantity:
            if self.quantity>self.item.available_stock:
                raise ValidationError(
                    {'quantity': "Quantity must be lesser than available stock"})
                
         if self.tentative_return_date:
            if self.tentative_return_date<=now:
                raise ValidationError(
                    {'tentative_return_date':'Tentative date must be greater or eqaul to todays date'}
                )

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)"""


