from django import forms

class DatePickerInput(forms.DateInput):
    input_type = 'datetime-local' 

class MinInteger(forms.NumberInput):
    input_min=0
